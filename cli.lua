package.path = package.path .. ";/usr/modules/?.lua"

local ccpt = require("jakobdev.ccpt")

local tArgs = {...}
if tArgs[1] == "install" then
    local ok, err = ccpt.installPackage(tArgs[2])
    if not ok then
        printError(err)
    end
elseif tArgs[1] == "remove" then
    local ok, err = ccpt.removePackage(tArgs[2])
    if not ok then
        printError(err)
    end
elseif tArgs[1] == "upgrade" then
    local tOutdated = ccpt.findOutdatedPackages()
    for _,i in ipairs(tOutdated) do
        ccpt.updatePackage(i)
    end
elseif tArgs[1] == "list" then
    for _,i in ipairs(ccpt.getPackageList()) do
        print(i)
    end
elseif tArgs[1] == "installed" then
    for _,i in ipairs(ccpt.getInstalledPackages()) do
        print(i)
    end
elseif tArgs[1] == "info" then
    local tInfo = {}
    local tPackage = ccpt.getPackageByName(tArgs[2])
    if not tPackage then
        printError('Package "' .. tArgs[2] .. '" was not found')
        return
    end
    table.insert(tInfo,{"Name:",tArgs[2]})
    table.insert(tInfo,{"Version:",tPackage.version})
    local tDependencies = ccpt.findDependencies(tArgs[2])
    if #tDependencies == 0 then
        table.insert(tInfo,{"Dependencies:","none"})
    else
        sDependencies = ""
        for _,i in ipairs(tDependencies) do
            sDependencies = sDependencies .. i .. " "
        end
        table.insert(tInfo,{"Dependencies:",sDependencies})
    end
    table.insert(tInfo,{"Description:",tPackage.description or "No description provided"})
    textutils.pagedTabulate(table.unpack(tInfo))
elseif tArgs[1] == "plugins" then
    local tPluginList = {}
    for _,i in pairs(ccpt.getPluginData()) do
        table.insert(tPluginList,i.functions.name())
    end
    textutils.pagedTabulate(tPluginList)
elseif tArgs[1] == "help" then
    local tText = {}
    table.insert(tText,{"install","Installs a package"})
    table.insert(tText,{"remove","Removes a package"})
    table.insert(tText,{"upgrade","Updates all packages"})
    table.insert(tText,{"list","Shows all available packages"})
    table.insert(tText,{"installed","Shows all installed packages"})
    table.insert(tText,{"info","Shows information about a package"})
    table.insert(tText,{"plugins","Shows all plugins"})
    table.insert(tText,{"help","Shows this help"})
    table.insert(tText,{"version","Shows the version of ccpt"})
    textutils.pagedTabulate(table.unpack(tText))
elseif tArgs[1] == "version" then
    print("CCPT Version " .. ccpt.version())
else
    print("Unknown command. Run ccpt help for a list of all commands.")
end
