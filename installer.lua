local function yesNoQuestion(bDefault)
    while true do
        local ev, key = os.pullEvent("key_up")
        if key == keys.y then
            return true
        elseif key == keys.n then
            return false
        elseif key == keys.enter and bDefault ~= nil then
            return bDefault
        end
    end
end

local function downloadFile(sUrl,sPath)
    local h = http.get(sUrl)
    local f = fs.open(sPath,"w")
    f.write(h.readAll())
    f.close()
    h.close()
end

print("Welcome to the CCPT Installer!")

if fs.exists("/usr/modules/jakobdev/ccpt.lua") then
    print("CCPT is already installed. Do you want to reinstall it? (y/n)")
    if not yesNoQuestion() then
        return
    end
end

if not http then
    printError("This installer needs http")
    return
end

print("Installing CCPT...")

downloadFile("https://gitlab.com/JakobDev/CCPT/-/raw/master/lib.lua","/usr/modules/jakobdev/ccpt.lua")
downloadFile("https://gitlab.com/JakobDev/CCPT/-/raw/master/cli.lua","/usr/bin/ccpt.lua")
downloadFile("https://gitlab.com/JakobDev/CCPT/-/raw/master/plugins/core.lua","/usr/share/ccpt/plugins/ccpt.lua")
downloadFile("https://gitlab.com/JakobDev/CCPT/-/raw/master/repofiles/main.txt","/etc/ccpt/repolist/main.txt")

package.path = package.path .. ";/usr/modules/?.lua"

local ccpt = require("jakobdev.ccpt")

ccpt.installPackage("ccpt:ccpt/ccpt")

print("Do you want to install the startup file? (Y/n)")
if yesNoQuestion(true) then
    ccpt.installPackage("ccpt:ccpt/ccpt-startup")
end
print("Do you want to install the packman plugin? (y/n)")
if yesNoQuestion() then
    ccpt.installPackage("ccpt:ccpt/ccpt-plugin-packman")
end

if fs.exists("/startup/ccpt.lua") and shell then
    shell.run("/startup/ccpt.lua")
end

print("Instalation complete!")