local tPackageProvider = {}
local tPackagePlugins = {}
local tPackages = {}
local sDataPath = "/var/ccpt"
local sCurrentNamespace = "none"
local tPluginData = {}
local tRepoList = {}

local function addPackageProvider(sNamespace,tFunctions,bSupportRepos)
    tPackageProvider[sNamespace] = tFunctions
    tPackageProvider[sNamespace]["supportRepos"] = bSupportRepos
end

local function addPackagePlugin(sName,tFunctions)
    tPackagePlugins[sName] = tFunctions
end

local function addPackage(sName,sRepo,tPackage,sNamespace)
    tPackage.name = sName
    sNamespace = sNamespace or sCurrentNamespace
    if not tPackages[sNamespace] then
        tPackages[sNamespace] = {}
    end
    if not tPackageProvider[sNamespace]["supportRepos"] then
        tPackages[sNamespace][sName] = tPackage
        return
    end
    if not tPackages[sNamespace][sRepo] then
        tPackages[sNamespace][sRepo] = {}
    end
    tPackages[sNamespace][sRepo][sName] = tPackage
end

local function updatePackageList()
    for k,v in pairs(tPackageProvider) do
        sCurrentNamespace = k
        v.updatePackageList(createPacklibEnv())
    end
    sCurrentNamespace = "none"
end

local function loadPlugin(sPath)
    local fnFile, sError = loadfile(sPath)
    if sError then
        print(sError)
        return
    end
    local tPluginFunctions = fnFile()
    local sID = fnFile()["id"]
    tPluginData[sID] = {}
    tPluginData[sID]["functions"] = tPluginFunctions
    tPluginFunctions["main"](createPacklibEnv())
end

local function loadPluginDirectory(sPath)
    for _,i in ipairs(fs.list(sPath)) do
        loadPlugin(fs.combine(sPath,i))
    end
end

local function splitPackageName(sName)
    --Check if the package has a namesapce. Otherwsise use the given or ccpt as namespace.
    local sNamespace = sDefaultNamespace or "ccpt"
    local sCustomNamespace, sRest = sName:match("([^:]+):([^:]+)")
    if sCustomNamespace then
        sNamespace = sCustomNamespace
        sName = sRest
    end
    --Check if the namespace exists
    if not tPackageProvider[sNamespace] then
        return nil, nil, nil
    end
    --Check if the namespace supports repos
    if not tPackageProvider[sNamespace]["supportRepos"] then
       return sNamespace, nil, sName
    end
    --Split into repo and package
    local sRepo, sPackname = sName:match("([^/]+)/([^/]+)")
    if not sRepo then
        --Search in the repos for the package
        for k,v in pairs(tPackages[sNamespace]) do
            if v[sName] then
                sRepo = k
                sPackname = sName
                break
            end
        end
    end
    return sNamespace, sRepo, sPackname
end

local function getPackageByName(sName,sDefaultNamespace)
    local sNamespace, sRepo, sPackname = splitPackageName(sName,sDefaultNamespace)
    if not sNamespace or not sPackname then
        return nil
    end
    if sRepo == nil then
        return tPackages[sNamespace][sPackname]
    end
    local package = tPackages[sNamespace][sRepo][sPackname]
    return package
end

local function getPackageInstallDataPath(sName,sDefaultNamespace)
    local sNamespace, sRepo, sPackname = splitPackageName(sName,sDefaultNamespace or "ccpt")
    if not sNamespace or not sPackname then
        return nil
    elseif sRepo then
        return fs.combine(sDataPath,"installed/" .. sNamespace .. "/" .. sRepo .. "/" .. sPackname)
    else
        return fs.combine(sDataPath,"installed/" .. sNamespace .. "/" .. sPackname)
    end
end

local function getPackageInstalledInfo(sName,sDefaultNamespace)
    local sPath = getPackageInstallDataPath(sName,sDefaultNamespace)
    if not sPath then
        return nil
    end
    local f = fs.open(sPath,"r")
    local data = textutils.unserialiseJSON(f.readAll())
    f.close()
    return data
end

local function findDependencies(sName)
    local tDependenciesCheck = {}
    local tPackage = getPackageByName(sName)
    for _,i in ipairs(tPackage.dependencies) do
        tDependenciesCheck[i] = true
        tList = findDependencies(i)
        for _,d in ipairs(tList) do
            tDependenciesCheck[d] = true
        end
    end
    local tDependenciesList = {}
    for k,_ in pairs(tDependenciesCheck) do
        table.insert(tDependenciesList,k)
    end
    return tDependenciesList
end

local function installSinglePackage(sName,bUserInstall)
    --local sNamespace, sRepo, sPackname = splitPackageName(sName)
    --local package = tPackages[sNamespace][sRepo][sPackname]
    local package = getPackageByName(sName,sNamespace)
    for _,i in ipairs(package.plugins) do
        if not tPackagePlugins[i] then
            return false, 'Could not find plugin "' .. i .. '"'
        end
        tPackagePlugins[i]["install"](package,createPacklibEnv())
    end
    local tPackageInfo = {version=package.version,user=bUserInstall or true}
    f = fs.open(getPackageInstallDataPath(sName),"w")
    f.write(textutils.serialiseJSON(tPackageInfo))
    f.close()
    local f = fs.open(fs.combine(sDataPath,"history.txt"),"a")
    f.writeLine("Installed " .. sName)
    f.close()
    return true
end

local function installPackage(sName)
    local tDependencies, err = findDependencies(sName)
    if err then
        return false, err
    end
    for _,i in ipairs(tDependencies) do
        local ok, err = installSinglePackage(i,false)
        if not ok then
            return false, err
        end
    end
    local ok, err = installSinglePackage(sName,true)
    if not ok then
        return false, err
    end
    return true
end

local function removePackage(sName)
    --local sNamespace, sRepo, sPackname = splitPackageName(sName)
    local sInstallInfoPath = getPackageInstallDataPath(sName)
    if not fs.exists(sInstallInfoPath) then
        return false, 'Package "' .. sName .. '" is not installed'
    end
    local tPackage = getPackageByName(sName)
    for _,i in ipairs(tPackage.plugins) do
        if not tPackagePlugins[i] then
            return false, 'Could not find plugin "' .. i .. '"'
        end
        tPackagePlugins[i]["remove"](tPackage,createPacklibEnv())
    end
    fs.delete(sInstallInfoPath)
    local f = fs.open(fs.combine(sDataPath,"history.txt"),"a")
    f.writeLine("Removed " .. sName)
    f.close()
    return true
end

local function updatePackage(sName)
    return installPackage(sName)
end

local function getPackageList()
    local tList = {}
    for namespace,repos in pairs(tPackages) do
        for reponame,repocon in pairs(repos) do
            if not tPackageProvider[namespace]["supportRepos"] then
                table.insert(tList,namespace .. ":" .. reponame)
            else
                for packname,_ in pairs(repocon) do
                    table.insert(tList,namespace .. ":" .. reponame .. "/" .. packname)
                end
            end
        end
    end
    return tList
end

local function getInstalledPackages()
    local tList = {}
    local sInstalledPath = fs.combine(sDataPath,"installed")
    if not fs.isDir(sInstalledPath) then
        return tList
    end
    for _,namespace in ipairs(fs.list(sInstalledPath)) do
        sNamespacePath = fs.combine(sInstalledPath,namespace)
        for _,reponame in ipairs(fs.list(sNamespacePath)) do
            local sRepoPath = fs.combine(sNamespacePath,reponame)
            if fs.isDir(sRepoPath) then
                for _,packname in ipairs(fs.list(sRepoPath)) do
                    table.insert(tList,namespace .. ":" .. reponame .. "/" .. packname)
                end
            else
                table.insert(tList,namespace .. ":" .. reponame)
            end
        end
    end
    return tList
end

local function findOutdatedPackages()
    local tOutdatedList = {}
    for _,i in ipairs(getInstalledPackages()) do
        local tPackage = getPackageByName(i)
        local tInstallInfo = getPackageInstalledInfo(i)
        if tPackage and tInstallInfo then
            if tPackage.version ~= tInstallInfo.version then
                table.insert(tOutdatedList,i)
            end
        end
    end
    return tOutdatedList
end

local function getPluginData()
    return tPluginData
end

local function loadRepoConfigFile(sPath)
    for sLine in io.lines(sPath) do
        if not sLine:find("#") then
            local tLine = {}
            for  sWord in sLine:gmatch("([^ ]+)") do
                table.insert(tLine,sWord)
            end
            if not tRepoList[tLine[1]] then
                tRepoList[tLine[1]] = {}
            end
            table.insert(tRepoList[tLine[1]],tLine)
        end
    end
end

local function loadRepoConfigDirectory(sPath)
    if not fs.isDir(sPath) then
        fs.makeDir(sPath)
    end
    for _,i in ipairs(fs.list(sPath)) do
        loadRepoConfigFile(fs.combine(sPath,i))
    end
end

local function getRepoList()
    return tRepoList
end

local function version()
    return "0.1"
end

local function startup()
    loadPluginDirectory("/usr/share/ccpt/plugins")
    loadRepoConfigDirectory("/etc/ccpt/repolist")
    updatePackageList()
end

function createPacklibEnv()
    return {
        addPackageProvider=addPackageProvider,
        addPackagePlugin=addPackagePlugin,
        addPackage=addPackage,
        updatePackageList=updatePackageList,
        loadPlugin=loadPlugin,
        loadPluginDirectory=loadPluginDirectory,
        getPackageByName=getPackageByName,
        findDependencies=findDependencies,
        installSinglePackage=installSinglePackage,
        installPackage=installPackage,
        removePackage=removePackage,
        getPackageList=getPackageList,
        getInstalledPackages=getInstalledPackages,
        findOutdatedPackages=findOutdatedPackages,
        getPluginData=getPluginData,
        loadRepoConfigFile,
        loadRepoConfigDirectory,
        getRepoList=getRepoList,
        version=version
    }
end

startup()

return createPacklibEnv()
