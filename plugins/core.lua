local function makeFilesPluginProvider()
    local tFunctions = {}
    function tFunctions.install(tPackage)
        for _,i in ipairs(tPackage.files) do
            local h = http.get(i[1])
            local f = fs.open(i[2],"w")
            f.write(h.readAll())
            f.close()
            h.close()
        end
    end
    function tFunctions.remove(tPackage)
        for _,i in ipairs(tPackage.files) do
            fs.delete(i[2])
        end
    end
    return tFunctions
end

local function makeMainPackageProvider()
    local tFunctions = {}
    local function parseRepoString(sRepo,sName,env)
        local tRepo = textutils.unserialiseJSON(sRepo)
        for k,v in pairs(tRepo) do
            env.addPackage(k,sName,v)
        end
    end
    local function parseOnlineRepo(sUrl,sName,env)
        local h = http.get(sUrl)
        parseRepoString(h.readAll(),sName,env)
        h.close()
    end
    local function parseRepoList(sUrl,env)
        h = http.get(sUrl)
        for sLine in h.readAll():gmatch("([^\n]+)") do
            local sRepoName, sRepoUrl = sLine:match("([^ ]+) ([^ ]+)")
            parseOnlineRepo(sRepoUrl,sRepoName,env)
        end
        h.close()
    end
    function tFunctions.updatePackageList(env)
        if not fs.isDir("/etc/ccpt/localrepos") then
            fs.makeDir("/etc/ccpt/localrepos")
        end
        for _,i in ipairs(fs.list("/etc/ccpt/localrepos")) do
            local f = fs.open(fs.combine("/etc/ccpt/localrepos",i),"r")
            parseRepoString(f.readAll(),i:sub(1,-6),env)
            f.close()
        end
        local tRepoList = env.getRepoList()
        for _,i in ipairs(tRepoList["ccpt-repo"] or {}) do
            parseOnlineRepo(i[2],i[3],env)
        end
        for _,i in ipairs(tRepoList["ccpt-repolist"] or {}) do
            parseRepoList(i[2],env)
        end
    end
    return tFunctions
end

local function main(env)
    env.addPackageProvider("ccpt",makeMainPackageProvider(),true)
    env.addPackagePlugin("files",makeFilesPluginProvider())
end

local function name()
    return "core"
end

local function id()
    return "core"
end

return {
    main=main,
    name=name,
    id=id
}
