local function downloadFile(sUrl,sPath)
    local h = http.get(sUrl)
    local f = fs.open(sPath,"w")
    f.write(h.readAll())
    f.close()
    h.close()
end

local function parsePackmanRepoFile(sRepo)
    local tPackageList = {}
    local sCurrentPackage = nil
    for line in sRepo:gmatch("([^\n]+)") do
        local property,hasValue,value = string.match(line, "^%s*([^=%s]+)%s*(=?)%s*(.-)%s*$")
        if property == "name" then
            sCurrentPackage = value
            tPackageList[value] = {}
        elseif property == "end" then
            sCurrentPackage = nil
        elseif property == nil then
            --nothing
        else
            tPackageList[sCurrentPackage][property] = value
        end
    end
    return tPackageList
end

local function addPackmanRepo(sRepo,sName,env)
    local tPackageList = parsePackmanRepoFile(sRepo)
    for k,v in pairs(tPackageList) do
        if v["type"] == "raw" then
            v["plugins"] = {"packman-base","packman-raw"}
        else
            v["plugins"] = {"packman-base"}
        end
        local tDependencies = {}
        for str in string.gmatch(v["dependencies"] or "none", "(%S+)") do
            table.insert(tDependencies,"packman:" .. str)
        end
        if tDependencies[1] ~= "packman:none" then
            v["dependencies"] = tDependencies
        else
            v["dependencies"] = {}
        end
        env.addPackage(k,sName,v)
    end
end

local function parseOnlineRepo(sUrl,sName,env)
    local h = http.get(sUrl)
    addPackmanRepo(h.readAll(),sName,env)
    h.close()
end

local function parseRepoList(sUrl,env)
    local h = http.get(sUrl)
    for sLine in h.readAll():gmatch("([^\n]+)") do
        local sRepoName, sRepoUrl = sLine:match("([^ ]+) ([^ ]+)")
        parseOnlineRepo(sRepoUrl,sRepoName,env)
    end
    h.close()
end

local function makePackmanPackageProvider()
    local tFunctions = {}
    function tFunctions.updatePackageList(env)
        local tRepoList = env.getRepoList()
        for _,i in ipairs(tRepoList["packman-repo"] or {}) do
            parseOnlineRepo(i[2],i[3],env)
        end
        for _,i in ipairs(tRepoList["packman-repolist"] or {}) do
            parseRepoList(i[2],env)
        end
    end
    return tFunctions
end

local function makeBasePluginProvider()
    local tFunctions = {}
    function tFunctions.install(tPackage)
        if tPackage["setup"] then
            shell.run(tPackage["setup"])
        end
    end
    return tFunctions
end

local function makeRawPluginProvider()
    local tFunctions = {}
    function tFunctions.install(tPackage)
        downloadFile(tPackage["url"],fs.combine(tPackage["target"] or "/usr/bin",tPackage["filename"]))
    end
    return tFunctions
end

local function main(env)
    env.addPackageProvider("packman",makePackmanPackageProvider(),true)
    env.addPackagePlugin("packman-base",makeBasePluginProvider())
    env.addPackagePlugin("packman-raw",makeRawPluginProvider())
end

local function name()
    return "packman"
end

local function id()
    return "packman"
end

return {
    main=main,
    name=name,
    id=id
}
