local sDownloadPath = "/usr/bin"

local function makeStdPluginProvider()
    local tFunctions = {}
    function tFunctions.install(tPackage)
        h = http.get(tPackage.url)
        f = fs.open(fs.combine(sDownloadPath,tPackage.name),"w")
        f.write(h.readAll())
        f.close()
        h.close()
    end
    function tFunctions.remove(tPackage)
        fs.delete(fs.combine(sDownloadPath,tPackage.name))
    end
    return tFunctions
end

local function parseRepoData(env,data)
    local f = loadstring(data)
    local std = {}
    setfenv(f,{std=std,pairs=pairs,colors=colors})
    f()
    for k,v in pairs(std.storeURLs) do
        local tPackageData = v
        tPackageData.plugins = {"std"}
        tPackageData.version = "unknown"
        tPackageData.dependencies = {}
        env.addPackage(k,"std",tPackageData)
    end
end

local function makeStdPackageProvider()
    local tFunctions = {}
    function tFunctions.updatePackageList(env)
        h = http.get("https://pastebin.com/raw/zVws7eLq")
        parseRepoData(env,h.readAll())
        h.close()
    end
    return tFunctions
end

local function main(env)
    env.addPackageProvider("std",makeStdPackageProvider(),false)
    env.addPackagePlugin("std",makeStdPluginProvider())
end

local function name()
    return "std"
end

local function id()
    return "std"
end

return {
    main=main,
    name=name,
    id=id
}
